# CodeNode

#### 介绍
本仓库主要用来记录自己学习前端的路径，希望对有缘人有帮助吧。


#### 三板斧基础

1.  [HTML结合JavaScript/CSS](https://blog.csdn.net/weixin_38289787/article/details/107462361)
2.  [详谈CSS选择器](https://blog.csdn.net/weixin_38289787/article/details/107464815)
3. [常见问题，感谢互联网](https://yangshun.github.io/front-end-interview-handbook/zh/html-questions/)

#### 微信公众号
**keepStarve**

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200809134649135.jpg)

微信公众号其实开通了几年了，文章没几篇。计划以后能周更，甚至日更，只是这以后还没有具体日期。主要不是用来分享技术，而是技术圈外的生活。

#### 大家加油 :)
